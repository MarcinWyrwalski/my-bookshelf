package pl.chomik.demo.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.servlet.http.HttpServletRequest;

@Configuration
@Order(1)
public class ApiSecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity
                .formLogin()
                    .successForwardUrl("/Index")
                    .permitAll()
                    .and()
                .authorizeRequests()
                    .antMatchers(HttpMethod.GET).hasRole("USER")
                    .antMatchers(HttpMethod.POST).hasRole("USER")
                    .anyRequest().authenticated();
    }

}

package pl.chomik.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.chomik.demo.domian.User;
import pl.chomik.demo.repository.UserRepository;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userRepository.findByName(s);

        if (user != null) {
            System.out.println(user.getName()+" user name");
            System.out.println(user.getPassword()+" user password");

            if (user.getRole()!=null) {
                System.out.println(user.getRole());
            } else {
                System.out.println(" user nie ma roli");
            }
            return new SecurityUserDetails(user);
        } else {
            throw new UsernameNotFoundException("No such user");
        }
    }

}

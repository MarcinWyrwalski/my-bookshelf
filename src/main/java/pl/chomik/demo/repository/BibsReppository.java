package pl.chomik.demo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.demo.domian.Bibs;

import java.util.Optional;

public interface BibsReppository extends CrudRepository<Bibs, Long>{
    Bibs findByisbnIssn(String str);

    Bibs findByid(Long id);
}

package pl.chomik.demo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.demo.domian.User;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByName(String s);
}

package pl.chomik.demo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.demo.domian.Book;

public interface BookRepository extends CrudRepository<Book, Long> {
}

package pl.chomik.demo.repository;

import org.springframework.data.repository.CrudRepository;
import pl.chomik.demo.domian.BibsDto;

public interface BibsDtoRepository extends CrudRepository<BibsDto, Long> {
}

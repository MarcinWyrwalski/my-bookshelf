package pl.chomik.demo.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.stereotype.Controller;
import pl.chomik.demo.domian.Book;

import java.io.IOException;
import java.net.URISyntaxException;

//import java.awt.print.Book;

@Controller
public class BookService {


    public Book getBookData(String isbn) throws URISyntaxException, IOException {
        System.out.println("inside getbokdata");

        ObjectMapper objectMapper = new ObjectMapper();
        URIBuilder uriBuilder = new URIBuilder("http://data.bn.org.pl/api/bibs.json");
        uriBuilder.addParameter("isbnIssn", isbn);

        Book book = objectMapper.readValue(uriBuilder.build().toURL(), Book.class);

        return book;
    }



}

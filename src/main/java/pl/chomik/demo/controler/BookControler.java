package pl.chomik.demo.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.chomik.demo.domian.Bibs;
import pl.chomik.demo.domian.BibsDto;
import pl.chomik.demo.domian.Book;
import pl.chomik.demo.domian.User;
import pl.chomik.demo.repository.BibsDtoRepository;
import pl.chomik.demo.repository.BibsReppository;
import pl.chomik.demo.repository.BookRepository;
import pl.chomik.demo.service.BookService;

import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class BookControler {

    @Autowired
    BookService bookService;

    @Autowired
    BookRepository bookRepository;

    @Autowired
    BibsReppository bibsReppository;

    @Autowired
    BibsDtoRepository bibsDtoRepository;



    @RequestMapping("books/addbook")
    public String displayAddBookPage() {
        System.out.println("inside controler");
        return "books/addbook";
    }

    @RequestMapping(value = "getBookFromWeb", method = RequestMethod.GET)
    public String processISBNSerachData(@RequestParam("isbn") String str, RedirectAttributes redirectAttributes) throws IOException, URISyntaxException {
        Book book = bookService.getBookData(str);

        Bibs[] bibs = book.getBibs();

        if (bibsReppository.findByisbnIssn(str) == null) {
            bibsReppository.save(bibs[0]);
        }

        Bibs foundBibs = bibsReppository.findByisbnIssn(str);
        Long id = foundBibs.getId();
        redirectAttributes.addFlashAttribute("bibs", foundBibs);


//        model.addAttribute("language", bibs[0].getLanguage());
//        model.addAttribute("subject", bibs[0].getSubject());
//        model.addAttribute("isbnIssn", bibs[0].getIsbnIssn());
//        model.addAttribute("author", bibs[0].getAuthor());
//        model.addAttribute("placeOfPublication", bibs[0].getPlaceOfPublication());
//        model.addAttribute("Title", bibs[0].getTitle());
//        model.addAttribute("publisher", bibs[0].getPublisher());
//        model.addAttribute("kind", bibs[0].getKind());
//        model.addAttribute("domain", bibs[0].getDomain());
//        model.addAttribute("formOfWork", bibs[0].getFormOfWork());
//        model.addAttribute("gerne", bibs[0].getGerne());
//        model.addAttribute("publicationYear", bibs[0].getPublicationYear());
////        model.addAttribute("id", bibs[0].getId());
//        model.addAttribute("Bibs", new Bibs());


        System.out.println(bibs);
        return "redirect:books/dispalybook";
    }

    @RequestMapping("/books/dispalybook")
    public String dispalyBook(Model model) {
//       Bibs bibs = bibsReppository.findByid(id);
//        System.out.println(bibs + " byID");
//        Optional<Bibs> byId = bibsReppository.findById(id);

//        mo
//        System.out.println(bibs + " bibs in dispalybook");
//
//        model.addAttribute("language", bibs.getLanguage());
//        model.addAttribute("subject", bibs.getSubject());
//        model.addAttribute("isbnIssn", bibs.getIsbnIssn());
//        model.addAttribute("author", bibs.getAuthor());
//        model.addAttribute("placeOfPublication", bibs.getPlaceOfPublication());
//        model.addAttribute("Title", bibs.getTitle());
//        model.addAttribute("publisher", bibs.getPublisher());
//        model.addAttribute("kind", bibs.getKind());
//        model.addAttribute("domain", bibs.getDomain());
//        model.addAttribute("formOfWork", bibs.getFormOfWork());
//        model.addAttribute("gerne", bibs.getGerne());
//        model.addAttribute("publicationYear", bibs.getPublicationYear());
//        model.addAttribute("id", bibs[0].getId());

        return "books/dispalybook";


    }

    @RequestMapping(name = "/Index")

    public String index() {
        return "Index";

    }

    @RequestMapping(value = "/books/saveBook", method = RequestMethod.GET)
    public String saveBook(@RequestParam("user") User user) {
        Bibs bibs1 = new Bibs();

        System.out.println("inside savebook");


        return "/Index";
    }

    @RequestMapping("/booklist")
    public String vievOwnBook(Model model){
        model.addAttribute("bibsdto", bibsDtoRepository.findAll());
        return "books/showOwnBooks";
    }


}




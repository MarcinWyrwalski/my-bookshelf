package pl.chomik.demo.controler.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.chomik.demo.domian.Book;
import pl.chomik.demo.domian.User;
import pl.chomik.demo.repository.UserRepository;
import pl.chomik.demo.service.BookService;

import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class UserControler {

    @Autowired
    UserRepository userRepository;
    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;


    @RequestMapping("/UserRegistrationPage")
    public String displayRegistrationPage(Model model) {
        model.addAttribute("User", new User());
        return "UserRegistrationPage.html";
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("USER") User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        System.out.println(user.getPassword() + " passwoerd ");

        if (user.getRole()!=null) {
            System.out.println(user.getRole());
        } else {
            System.out.println(" user nie ma roli");
        }
        userRepository.save(user);
        System.out.println("save ok");
    return  "index";
    }


}
